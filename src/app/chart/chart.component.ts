import { Component, OnInit } from '@angular/core';
import { DataService} from '../services/data.service';
import * as d3 from 'd3';


@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
data:any
  constructor( private dataService:DataService) {
   }

  ngOnInit() {
    this.data=this.dataService.chartData
    console.log('this.data: ', this.data);
    this.getChart()
 }

 getChart(){
       // set the dimensions and margins of the graph
var margin = { top: 20, right: 20, bottom: 30, left: 50 },
width = 960 - margin.left - margin.right,
height = 500 - margin.top - margin.bottom;

// parse the totalScore / time
// var parseTime = d3.timeParse("%d-%b-%y");

// var x = d3.scaleBand().rangeRound([0, width]).padding(0.1),
//     y = d3.scaleLinear().rangeRound([height, 0]);

// append the svg obgect to the body of the page
// appends a 'group' element to 'svg'
// moves the 'group' element to the top left margin
var svg = d3
.select("#divId")
.append("svg")
.attr("width", width + margin.left + margin.right)
.attr("height", height + margin.top + margin.bottom)
.append("g")
.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// get the data
// d3.json("daa.json", function(error, data) {
// if (error) throw error;

var data = this.data.candidateScoreList;
console.log('data: ', data);
var data1 = [];
var dummy_x = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
var pickPoint;
data.forEach(function(d, i) {
  console.log(d);
  data1.push({
    x: String(d.totalScore.toFixed(0)),
    y: String(Math.floor(Math.random() * (100 - 0 + 1)) + 0), //.sort(),
    name: d.name ? d.name : ""
  });
  if (d.name) {
    pickPoint = d.totalScore.toFixed(0);
  }
});

data = data1;
data.sort(function(a, b) {
  return a.x - b.x;
});
console.log(data1);

var x = d3
  .scaleBand()
  .rangeRound([0, width])
  .domain(
    data.map(function(d) {
      return d.x;
    })
  );

var y = d3
  .scaleLinear()
  .domain([0, 200])
  .range([height, 0]);


// Add the x Axis
var xAxis = svg
  .append("g")
  .attr("transform", "translate(0," + height + ")")
  .call(d3.axisBottom(x));

// Add the y Axis
var yAxis = svg.append("g").call(d3.axisLeft(y));

// define the area
var area = d3
  .area()
  .curve(d3.curveBasis)
  .x(function(d) {
    return x(d.x);
  })
  .y0(height)
  .y1(function(d) {
    return y(d.y)
  });

// define the line
var valueline = d3
  .line()
  .curve(d3.curveBasis)
  .x(d => x(d.x))
  .y(d => y(d.y));

// add the area
svg
  .append("path")
  .data([data])
  //    .data(data)
  .attr("class", "area")
  .attr("d", area)

// add the valueline path.
var lines = svg
  .append("path")
  .data([data])
  // .data(data)
  .attr("class", "line11")
  .attr("d", valueline)
  .attr("fill", function(d, i) {
    console.log(d, i);
  });

var point = parseInt(pickPoint);

svg
  .append("line")
  .attr("x1", function(data,i){ console.log(data,i); return 423 + 40}) //x(point))//-margin.left)
  .attr("x2", 423 + 40) //x(point))//-margin.left)
  .attr("y1", 300) //height+1)
  .attr("y2", height + 1)
  .attr("stroke", "#2b46bf")
  .attr("stroke-width", "3px");

/* Add circles in the line */
svg
.append('circle')
.attr("cx", 463)//
.attr("cy",  function(d,i){
  return 300;
})//
.attr("r", 10)
.attr("fill", "#2b46bf")

// });
 
 }

}
