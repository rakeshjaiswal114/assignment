import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public chartData
  constructor() {
    this.chartData={
      "candidateScoreList": [
        {
          "totalScore": 0
        },
        {
          "totalScore": 48.214285714285715
        },
        {
          "totalScore": 42.857142857142854
        },
        {
          "totalScore": 19.642857142857142
        },
        {
          "totalScore": 46.42857142857143
        },
        {
          "totalScore": 73.21428571428572
        },
        {
          "totalScore": 85.71428571428572
        },
        {
          "totalScore": 39.285714285714285
        },
        {
          "totalScore": 14.285714285714285
        },
        {
          "totalScore": 80.35714285714286
        },
        {
          "totalScore": 41.07142857142857
    
        },
        {
          "totalScore": 66.07142857142857
        },
        {
          "totalScore": 73.21428571428572
        },
        {
          "totalScore": 85.71428571428572
        },
        {
          "totalScore": 66.07142857142857
        },
        {
          "totalScore": 73.21428571428572
        },
        {
          "totalScore": 39.285714285714285
        },
        {
          "totalScore": 71.42857142857143
        },
        {
          "totalScore": 41.07142857142857
        },
        {
          "totalScore": 71.42857142857143
        },
        {
          "name": "Khushroo",
          "totalScore": 49.07142857142857
        },
        {
          "totalScore": 25
        },
        {
          "totalScore": 67.85714285714286
        },
        {
          "totalScore": 53.57142857142857
        },
        {
          "totalScore": 41.07142857142857
        },
        {
          "totalScore": 60.714285714285715
        },
        {
          "totalScore": 85.71428571428572
        },
        {
          "totalScore": 80.35714285714286
        },
        {
          "totalScore": 41.07142857142857
        },
        {
          "totalScore": 78.57142857142857
        },
        {
          "totalScore": 19.642857142857142
        },
        {
          "totalScore": 78.57142857142857
        },
        {
          "totalScore": 33.92857142857143
        },
        {
          "totalScore": 46.42857142857143
        },
        {
          "totalScore": 66.07142857142857
        },
        {
          "totalScore": 46.42857142857143
        },
        {
          "totalScore": 46.42857142857143
        },
        {
          "totalScore": 25
        },
        {
          "totalScore": 53.57142857142857
        },
        {
          "totalScore": 58.92857142857143
        },
        {
          "totalScore": 85.71428571428572
        }
      ]
    }
   }
}
